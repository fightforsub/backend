create table accountevent
(
	id uuid not null
		constraint accountevent_pkey
			primary key,
    accountid varchar(255) not null,
	eventid varchar(255) not null,
	subbed boolean not null
);