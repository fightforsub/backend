package com.zerator.fightforsub.entity;

import io.quarkus.hibernate.orm.panache.PanacheEntityBase;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.UUID;

@Entity
public class AccountEvent extends PanacheEntityBase {
    @Id
    @GeneratedValue
    @Column(nullable = false)
    public UUID id;

    @Column(nullable = false)
    public String accountId;

    @Column(nullable = false)
    public String eventId;

    @Column(nullable = false)
    public Boolean subbed;

    public static void create(String accountId, String eventId) {
        var accountEvent = new AccountEvent();
        accountEvent.accountId = accountId;
        accountEvent.eventId = eventId;
        accountEvent.subbed = true;
        accountEvent.persist();
    }
}
