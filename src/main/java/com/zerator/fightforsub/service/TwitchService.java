package com.zerator.fightforsub.service;

import com.github.twitch4j.TwitchClient;
import com.github.twitch4j.TwitchClientBuilder;
import com.github.twitch4j.common.exception.NotFoundException;
import io.quarkus.runtime.StartupEvent;
import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.eclipse.microprofile.opentracing.Traced;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Observes;

@ApplicationScoped
@Traced
public class TwitchService {
    private TwitchClient twitchClient;

    @ConfigProperty(name = "twitch.client-id")
    String twitchClientId;

    @ConfigProperty(name = "twitch.client-secret")
    String twitchClientSecret;

    public void onStart(@Observes StartupEvent e) {
        twitchClient = TwitchClientBuilder.builder()
                .withEnableHelix(true)
                .withClientId(twitchClientId)
                .withClientSecret(twitchClientSecret)
                .build();
    }

    public boolean isSubbed(String authToken, String viewerId, String winnerId) {
        try {
            twitchClient.getHelix().checkUserSubscription(authToken, viewerId, winnerId).execute();
            return true;
        } catch (NotFoundException e) {
            return false;
        }
    }
}
