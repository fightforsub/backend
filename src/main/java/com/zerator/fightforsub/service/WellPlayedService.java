package com.zerator.fightforsub.service;

import com.zerator.fightforsub.dto.WpAccount;
import org.eclipse.microprofile.opentracing.Traced;
import org.eclipse.microprofile.rest.client.inject.RestClient;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.util.HashMap;
import java.util.Map;

@ApplicationScoped
@Traced
public class WellPlayedService {
    @Inject
    @RestClient
    WellPlayedApi wellPlayedApi;

    public WpAccount getCurrentAccount(String authorizationHeader) {
        return wellPlayedApi.getCurrentAccount(authorizationHeader);
    }

    public String findEventWinnerTwitchId(String authorizationHeader, String organisationId, String eventId) {
        var wpEvent = wellPlayedApi.getEvent(authorizationHeader, organisationId, eventId);
        var wpRounds = wellPlayedApi.getRounds(authorizationHeader, eventId, 0, 1000);
        if (wpRounds.content.isEmpty()) {
            return null;
        }

        Map<String, TeamEntry> combinedScores = new HashMap<>();
        wpRounds.content.stream()
                .map(wpRound -> wellPlayedApi.getScores(authorizationHeader, wpRound.id, 0, 1000))
                .forEach(wpRound -> wpRound.content
                        .forEach(wpScore -> {
                            var combinedScore = combinedScores.getOrDefault(wpScore.teamId,
                                    new TeamEntry(0, wpScore.teamId, wpScore.team.members, wpEvent.rankingType));
                            combinedScore.score += wpScore.score;
                            combinedScores.put(wpScore.teamId, combinedScore);
                        }));

        if (combinedScores.isEmpty()) {
            return null;
        }

        var winnerTeamEntry = combinedScores.values().stream().sorted().findFirst().get();

        if (winnerTeamEntry.members.isEmpty()) {
            return null;
        }

        return wellPlayedApi.getTwitchIdentity(authorizationHeader, winnerTeamEntry.members.get(0)).identity._id;
    }

    public String getTwitchId(String authorizationHeader, String id) {
        var wpIdentity = wellPlayedApi.getTwitchIdentity(authorizationHeader, id);
        return wpIdentity.identity._id;
    }
}
