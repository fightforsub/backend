package com.zerator.fightforsub.service;

import com.zerator.fightforsub.dto.*;
import org.apache.http.HttpHeaders;
import org.eclipse.microprofile.opentracing.Traced;
import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;

import javax.enterprise.context.ApplicationScoped;
import javax.ws.rs.*;

@RegisterRestClient
@ApplicationScoped
@Traced
public interface WellPlayedApi {
    @GET
    @Path("/accounts/me")
    WpAccount getCurrentAccount(@HeaderParam(HttpHeaders.AUTHORIZATION) String authorizationHeader);

    @GET
    @Path("/accounts/identities/a/{accountId}/TWITCH")
    WpIdentity getTwitchIdentity(@HeaderParam(HttpHeaders.AUTHORIZATION) String authorizationHeader, @PathParam("accountId") String accountId);

    @GET
    @Path("/organisations/{organisationId}/events/{eventId}")
    WpEvent getEvent(@HeaderParam(HttpHeaders.AUTHORIZATION) String authorizationHeader, @PathParam("organisationId") String organisationId, @PathParam("eventId") String eventId);

    @GET
    @Path("/rounds/{eventId}/rounds")
    WpRounds getRounds(@HeaderParam(HttpHeaders.AUTHORIZATION) String authorizationHeader, @PathParam("eventId") String eventId, @QueryParam("page") int page, @QueryParam("size") int size);

    @GET
    @Path("/rank/score/{roundId}")
    WpScores getScores(@HeaderParam(HttpHeaders.AUTHORIZATION) String authorizationHeader, @PathParam("roundId") String roundId, @QueryParam("page") int page, @QueryParam("size") int size);
}
