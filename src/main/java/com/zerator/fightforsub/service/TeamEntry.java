package com.zerator.fightforsub.service;

import com.zerator.fightforsub.dto.WpEventRankingType;

import java.util.List;

public class TeamEntry implements Comparable<TeamEntry> {
    public int score;
    public String teamId;
    public List<String> members;
    public WpEventRankingType rankingType;

    public TeamEntry(int score, String teamId, List<String> members, WpEventRankingType rankingType) {
        this.score = score;
        this.teamId = teamId;
        this.members = members;
        this.rankingType = rankingType;
    }

    @Override
    public int compareTo(TeamEntry o) {
        return Integer.compare(score, o.score) * (rankingType == WpEventRankingType.SCORE_ASCENDING ? 1 : -1);
    }
}
