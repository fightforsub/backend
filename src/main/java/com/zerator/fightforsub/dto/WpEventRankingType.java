package com.zerator.fightforsub.dto;

public enum WpEventRankingType {
    SCORE_ASCENDING,
    SCORE_DESCENDING
}
