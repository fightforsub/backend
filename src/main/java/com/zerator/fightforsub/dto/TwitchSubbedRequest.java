package com.zerator.fightforsub.dto;

import javax.validation.constraints.NotBlank;

public class TwitchSubbedRequest {
    @NotBlank
    public String twitchToken;

    @NotBlank
    public String eventId;
}
