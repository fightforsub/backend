package com.zerator.fightforsub.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class WpRounds {
    public List<WpRound> content;
}
