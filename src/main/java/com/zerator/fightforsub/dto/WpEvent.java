package com.zerator.fightforsub.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class WpEvent {
    public WpEventRankingType rankingType;
}
