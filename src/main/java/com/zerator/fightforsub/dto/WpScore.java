package com.zerator.fightforsub.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class WpScore {
    public int score;
    public String teamId;
    public WpTeam team;

    public WpScore(int score, String teamId, WpTeam team) {
        this.score = score;
        this.teamId = teamId;
        this.team = team;
    }
}
