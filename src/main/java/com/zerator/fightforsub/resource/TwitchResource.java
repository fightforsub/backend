package com.zerator.fightforsub.resource;

import com.zerator.fightforsub.dto.TwitchSubbedRequest;
import com.zerator.fightforsub.entity.AccountEvent;
import com.zerator.fightforsub.service.TwitchService;
import com.zerator.fightforsub.service.WellPlayedService;

import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/twitch")
public class TwitchResource {
    @Inject
    WellPlayedService wellPlayedService;

    @Inject
    TwitchService twitchService;

    @POST
    @Path("subbed")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Transactional
    public Response subbed(@HeaderParam(HttpHeaders.AUTHORIZATION) String authorizationHeader, @Valid @NotNull TwitchSubbedRequest request) {
        var wpAccount = wellPlayedService.getCurrentAccount(authorizationHeader);
        String accountTwitchId = wellPlayedService.getTwitchId(authorizationHeader, wpAccount.id);
        if (accountTwitchId == null) {
            return Response.status(400).build();
        }

        String winnerTwitchId = wellPlayedService.findEventWinnerTwitchId(authorizationHeader, wpAccount.organisation, request.eventId);
        if (winnerTwitchId == null) {
            return Response.status(400).build();
        }

        if (!twitchService.isSubbed(request.twitchToken, accountTwitchId, winnerTwitchId)) {
            return Response.status(400).build();
        }

        AccountEvent.create(wpAccount.id, request.eventId);
        return Response.ok().build();
    }
}