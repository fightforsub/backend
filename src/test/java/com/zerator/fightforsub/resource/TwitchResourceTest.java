package com.zerator.fightforsub.resource;

import com.zerator.fightforsub.dto.TwitchSubbedRequest;
import com.zerator.fightforsub.util.WireMockExtensions;
import io.quarkus.test.common.QuarkusTestResource;
import io.quarkus.test.junit.QuarkusTest;
import org.apache.http.HttpHeaders;
import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.given;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

@QuarkusTest
@QuarkusTestResource(WireMockExtensions.class)
public class TwitchResourceTest {
    @Test
    public void testTwichSubbedValidation() {
        var request = new TwitchSubbedRequest();
        request.eventId = "eventId";

        given()
                .header(HttpHeaders.AUTHORIZATION, "Bearer Unauthorized")
                .contentType(APPLICATION_JSON)
                .when().body(request).post("/twitch/subbed")
                .then()
                .statusCode(400);
    }

    @Test
    public void testTwichSubbedUnauthorized() {
        var request = new TwitchSubbedRequest();
        request.eventId = "eventId";
        request.twitchToken = "twitchToken";

        given()
                .header(HttpHeaders.AUTHORIZATION, "Bearer Unauthorized")
                .contentType(APPLICATION_JSON)
                .when().body(request).post("/twitch/subbed")
                .then()
                .statusCode(401);
    }

    @Test
    public void testTwichSubbedOk() {
        var request = new TwitchSubbedRequest();
        request.eventId = "eventMock";
        request.twitchToken = "twitchToken";

        given()
                .header(HttpHeaders.AUTHORIZATION, "Bearer MyToken")
                .contentType(APPLICATION_JSON)
                .when().body(request).post("/twitch/subbed")
                .then()
                .statusCode(200);
    }
}