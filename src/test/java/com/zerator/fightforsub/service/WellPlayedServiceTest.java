package com.zerator.fightforsub.service;

import com.zerator.fightforsub.util.WireMockExtensions;
import io.quarkus.test.common.QuarkusTestResource;
import io.quarkus.test.junit.QuarkusTest;
import org.junit.jupiter.api.Test;

import javax.inject.Inject;
import javax.ws.rs.WebApplicationException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

@QuarkusTest
@QuarkusTestResource(WireMockExtensions.class)
public class WellPlayedServiceTest {
    @Inject
    WellPlayedService wellPlayedService;

    @Test
    public void testGetWpAccount() {
        var wpAccount = wellPlayedService.getCurrentAccount("MyToken");
        assertEquals("idMock", wpAccount.id);
        try {
            wellPlayedService.getCurrentAccount("Unauthorized");
            fail();
        } catch (WebApplicationException e) {
            assertEquals(401, e.getResponse().getStatus());
        }
    }

    @Test
    public void testFindEventWinnerTwitchId() {
        String winnerTwitchId = wellPlayedService.findEventWinnerTwitchId(null, "organisationMock", "eventMock");
        assertEquals("twitchIdUser1", winnerTwitchId);
    }
}
