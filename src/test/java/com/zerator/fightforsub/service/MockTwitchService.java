package com.zerator.fightforsub.service;

import io.quarkus.test.Mock;

@Mock
public class MockTwitchService extends TwitchService {
    @Override
    public boolean isSubbed(String authToken, String viewerId, String winnerId) {
        return viewerId.equals("twitchIdMock") && winnerId.equals("twitchIdUser1");
    }
}
