package com.zerator.fightforsub.util;

import com.github.tomakehurst.wiremock.WireMockServer;
import io.quarkus.test.common.QuarkusTestResourceLifecycleManager;

import java.util.Collections;
import java.util.Map;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.options;

public class WireMockExtensions implements QuarkusTestResourceLifecycleManager {  
	private WireMockServer wireMockServer;

	@Override
	public Map<String, String> start() {
		wireMockServer = new WireMockServer(options().port(8089));
		wireMockServer.start();
		configureFor("localhost", 8089);

		stubFor(get(urlEqualTo("/accounts/me"))
				.withHeader("Authorization", containing("Unauthorized"))
				.willReturn(aResponse()
						.withStatus(401)));

		stubFor(get(urlEqualTo("/accounts/me"))
				.withHeader("Authorization", containing("MyToken"))
				.willReturn(aResponse()
						.withHeader("Content-Type", "application/json")
						.withBody("{\n" +
								"  \"id\": \"idMock\",\n" +
								"  \"passportId\": \"passportIdMock\",\n" +
								"  \"username\": \"usernameMock\",\n" +
								"  \"organisation\": \"organisationMock\"\n" +
								"}")));

		stubFor(get(urlEqualTo("/organisations/organisationMock/events/eventMock"))
				.willReturn(aResponse()
						.withHeader("Content-Type", "application/json")
						.withBody("{\"rankingType\":\"SCORE_ASCENDING\"}")));

		stubFor(get(urlEqualTo("/rounds/eventMock/rounds?page=0&size=1000"))
				.willReturn(aResponse()
						.withHeader("Content-Type", "application/json")
						.withBody("{\"content\": [{\n" +
								"      \"id\": \"roundMock1\"\n" +
								"    }, {\n" +
								"      \"id\": \"roundMock2\"\n" +
								"    }]}")));

		stubFor(get(urlEqualTo("/rank/score/roundMock1?page=0&size=1000"))
				.willReturn(aResponse()
						.withHeader("Content-Type", "application/json")
						.withBody("{\"content\": [" +
								"{\"teamId\": \"teamMock2\", \"score\": 2, \"team\": {\"members\": [\"accountMock2\"]}}," +
								"{\"teamId\": \"teamMock3\", \"score\": 3, \"team\": {\"members\": [\"accountMock3\"]}}," +
								"{\"teamId\": \"teamMock1\", \"score\": 1, \"team\": {\"members\": [\"accountMock1\"]}}" +
								"]}")));

		stubFor(get(urlEqualTo("/rank/score/roundMock2?page=0&size=1000"))
				.willReturn(aResponse()
						.withHeader("Content-Type", "application/json")
						.withBody("{\"content\": [" +
								"{\"teamId\": \"teamMock2\", \"score\": 4, \"team\": {\"members\": [\"accountMock2\"]}}," +
								"{\"teamId\": \"teamMock1\", \"score\": 1, \"team\": {\"members\": [\"accountMock1\"]}}," +
								"{\"teamId\": \"teamMock3\", \"score\": 8, \"team\": {\"members\": [\"accountMock3\"]}}" +
								"]}")));

		stubFor(get(urlEqualTo("/accounts/identities/a/idMock/TWITCH"))
				.willReturn(aResponse()
						.withHeader("Content-Type", "application/json")
						.withBody("{\"identity\": {\"_id\": \"twitchIdMock\"}}")));

		stubFor(get(urlEqualTo("/accounts/identities/a/accountMock1/TWITCH"))
				.willReturn(aResponse()
						.withHeader("Content-Type", "application/json")
						.withBody("{\"identity\": {\"_id\": \"twitchIdUser1\"}}")));

		return Collections.singletonMap("quarkus.rest-client.\"com.zerator.fightforsub.service.WellPlayedApi\".url", wireMockServer.baseUrl());
	}

	@Override
	public void stop() {
		if (null != wireMockServer) {
			wireMockServer.stop();  
		}
	}
}